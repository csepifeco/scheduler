#scheduler.php - v0.1
## License
This project is licensed under GNU GPL v2.0 - further info here http://choosealicense.com/licenses/gpl-2.0/
##Introduction
This is a custom php script to help you organise meetings during a project.

##Usage
You can use it with PHP 5+. Simple copy the script to your drive, enter its directory and use the following syntax.
	
	Syntax:
		> php scheduler.php "day for meeting" "day for test" "duration in months"

	Example of usage:
		> php scheduler.php "14" "last" "6"
		> php scheduler.php "8" "14" "24"
		> php scheduler.php "25" "14" "10"

## Motivation
I enjoy coding.

##Release Notes
	
	Release date - 2014 Dec 14
	What's New in v0.1:
		Added basic validation to user entries
		The user can choose their desired date as they are not tied to fixed values.
			If the user choose a meeting day that is in the past then the script asks whether the test should be delayed