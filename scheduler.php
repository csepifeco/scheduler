<?php
//	========
//	INTRODUCTION
//	========
//
//	scheduler.php is a command line script that can be used to determine which days to held meetings and test days for certain amount of months
//	The results will be saved in the CSV file called "mySchedule_UID.csv" in the same folder as the script where UID is generated.
//
//	Syntax:			> php scheduler.php "day for meeting" "day for test" "duration in months"
//
//	Example of usage:		> php scheduler.php "14" "last" "6"
//							> php scheduler.php "8" "14" "24"
//							> php scheduler.php "25" "14" "10"
//	
//
//	Important Notes:
//		You will have to launch the script locally from it's own folder in order to create the CSVs.
//		Only basic validation has been added at this stage.
//	
//	
//	Created by Ferenc Csepregi - 2014 Dec
//	Version - 0.1
//
//	========

// === ARGUMENTS ===============================
// do some basic argument validation
$c = count($argv);
if($c < 4 || $argv[1] == "" || $argv[2] == "" || $argv[3] == ""){
	exit("====\nERROR - Missing arguments.\n====\nscheduler.php requires 3 arguments from which NONE of it can be empty!\nIf you need help please refer to the documentation");
} else if($c > 4){
	exit("====\nERROR - Too many arguments were passed in.\n====\nHELP - scheduler.php requires 3 arguments from which NONE of it can be empty!\nIf you need help please refer to the documentation");
} else {
	// assign arguments to variables
	$meetingDay = $argv[1]; // when the meeting should be held
	$testDay = $argv[2]; // when the test should be scheduled
	$duration = $argv[3]; // duration in months
	// if the meeting Day could not be fit into the month then throw an error
	if ($meetingDay > date('t') || $meetingDay <= 0){
		exit("====\nERROR - Could not determine the first meeting because the current Month doesn't have $meetingDay days.\n====\nHELP - Please provide a number between 1 and " . date('t'));
	}
	//echo "\n\n\n$testDay\n\n\n";
	if ($testDay == 'last') {}
	else if ($testDay <= 0 || $testDay > date('t') ) {
		exit("====\nERROR - Could not determine the first test because the current Month doesn't have $testDay days.\n====\nHELP - Please provide the word 'last' for the last available day of the month or a number between 1 and " . date('t'));
	}
}

// === FUNCTIONS ===============================


// 	==== goSchedule function ==================
//	
// you can pass 3 arguments
//	First Argument - $meetingDay - day of meeting for eah month
//	Second Argument - $testDay - day of test:
//	Third Argument - $d - duration in months
//	Fourth Argument - $fileName - desired CSV file name.

function goSchedule($meetingDay,$testDay,$d,$fileName){
	$startDate = setStartDate($meetingDay,$testDay);
	$dayDiff = $testDay - $meetingDay;
	$schedule = array (
		array('Month', 'Mid Month Meeting Date', 'End of Month Testing Date')
	);
	
	// Loop through the moths and fill the array
	for ($x = 1; $x <= $d; $x++) {    
		// get current Month
		$month = date('F',strtotime($startDate . "+" . ($x-1) . " months"));
	
		// get meeting date for the Month
		$meetingDate = date('d-m-Y',strtotime($startDate . "+" . ($x-1) . " months"));
		// reschedule if it falls on weekend
		$meetingDate = meetingRS($meetingDate);
	
		// get testing date for the Month
		// if user chooses the last day of month then assign the last day of the current month
		if($testDay == "last"){$testDate = date('t-m-Y',strtotime($meetingDate));}
		// otherwise append the extra days to the meeting's date
		else {$testDate = date('d-m-Y',strtotime($meetingDate . "+$dayDiff days"));}
		// reschedule if it falls on Friday or weekend
		$testDate = testRS($testDate);
	
		// Saving values to array
		$schedule[$x] = array($month,$meetingDate,$testDate);
	}
	

		// export to CSV
		exportCSV($fileName,$schedule);
}

// 	==== exportCSV function ====================
function exportCSV($fileName,$myArray){
	$stamp = date("ymdHis");
	// open file to write
	$fp = fopen("$fileName" . "_" . "$stamp.csv", 'w');
	// write
	foreach ($myArray as $fields) {
		fputcsv($fp, $fields);
	}
	// then close the file
	fclose($fp);	
}

// 	==== meetingRS function ====================
function meetingRS($dateToCheck){
	$gotDay = date('D', strtotime($dateToCheck));
	$x = 0;
	switch ($gotDay) {
		case "Sat":
			$x = 2;
			break;
		case "Sun":
			$x = 1;
			break;
	}
	$dateToCheck = date('D  d-m-Y',strtotime($dateToCheck . "+$x days"));
	return $dateToCheck;	
}

// 	==== testRS function ======================
function testRS($dateToCheck){
	$gotDay = date('D', strtotime($dateToCheck));
	$x = 0;
	switch ($gotDay) {
		case "Fri":
			$x = 1;
			break;
		case "Sat":
			$x = 2;
			break;
		case "Sun":
			$x = 3;
			break;
	}
	$dateToCheck = date('D  d-m-Y',strtotime($dateToCheck . "-$x days"));
	return $dateToCheck;
}

// 	==== setStartMonth function ===============
function setStartDate($meeting){
	$today = date('d');
	$startMonth = date('m');
	$startYear = date('Y');
	$startDate = "$meeting-$startMonth-$startYear";
	if (skipMonth($meeting,$today)){$startDate = date('d-m-Y',strtotime($startDate . "+1 months"));}
	return $startDate;
}

// 	==== skipMonth function ===================
function skipMonth($meeting,$today){
	if ($meeting > $today) {
		return FALSE;
	} else {
		return promptUser($meeting);
	}
}

// 	==== promptUser function ===================
function promptUser($date){
	echo "Hmm, looks like we already have passed the date for your meeting ($date) this month.\nWould you like to wait with the tests until the first meeting is held?\nType 'Y' to wait or 'N' to schedule a test for this month anyway: ";
	$handle = fopen ("php://stdin","r");
	$line = fgets($handle);
	$answer = trim($line);
	if($answer == 'Y' || $answer == 'y'){
		echo "Thank you, the test day for this month has been SKIPPED!";
		return TRUE; // which means we will skip to the next meeting without scheduling a test date
	} else {
		echo "Thank you, the test day for this month will be SCHEDULED!";
		return FALSE; // which means we will skip to the next meeting without scheduling a test date
	}
}

// create the schedule array and save it in a file
goSchedule($meetingDay,$testDay,$duration,'mySchedule');

?>